"""
Converts an OWASP Dependency-Check report to GitLab Dependency Scanner format.

Note that as of June 2020, the GitLab docs for the format of the report are incorrect:
https://gitlab.devops.geointservices.io/help/user/application_security/dependency_scanning/index.md

Nick Livingston, June 2020
"""


import json
import os
import uuid


class ODCtoGitLabConverter:
    """Converts Dependency-Check reports to GitLab format."""
    depcheck_report = None
    vulnerabilities = list()
    added_vulns = list()

    def __init__(self):
        """Load the report."""
        with open(f'{os.getcwd()}/dependency-check-report.json', 'r') as f:
            self.depcheck_report = json.load(f)

    def processs_depcheck_report(self):
        """Find vulnerabilities in the report and convert them."""
        for dependency in self.depcheck_report['dependencies']:
            if 'vulnerabilities' in dependency.keys():
                # Get the dependency information for all the vulnerabilities
                new_dependency = Dependency()
                if 'md5' in dependency.keys():
                    new_dependency.id = dependency['md5']
                else:
                    new_dependency.id = uuid.uuid4().hex
                new_dependency.name = dependency['evidenceCollected']['productEvidence'][0]['value']
                new_dependency.version = dependency['evidenceCollected']['versionEvidence'][0]['value']
                new_dependency.file = dependency['fileName']

                for vulnerability in dependency['vulnerabilities']:
                    # Process each vulnerability for this dependency
                    new_vulnerability = Vulnerability()
                    new_vulnerability.category = 'dependency_scanning'
                    new_vulnerability.name = vulnerability['name']
                    new_vulnerability.description = vulnerability['description']
                    new_vulnerability.severity = vulnerability['severity'].title()
                    if new_vulnerability.severity == 'Moderate':
                        new_vulnerability.severity = 'Medium'
                    new_vulnerability.scanner = {
                        'id': vulnerability['source'],
                        'name': vulnerability['source'].title()
                    }
                    new_vulnerability.location = {
                        'file': new_dependency.file,
                        'dependency': {
                            'package': {
                                'name': new_dependency.name
                            },
                            'version': new_dependency.version
                        }
                    }

                    # Come up with an identifier
                    new_vulnerability.identifiers = list()
                    new_vulnerability.identifiers.append(
                        {
                            'type': 'cwe',
                            'name': 'OWASP Dependency-Check',
                            'value': 'no-value-provided',
                         }
                    )

                    new_vulnerability.links = list()
                    for reference in vulnerability['references']:
                        if 'url' in reference.keys():
                            name = reference['name']
                            url = reference['url']
                        else:
                            # Has "source" and "name" instead
                            new_vulnerability.name = reference['source']
                            name = reference['source']
                            if 'http' in reference['name']:
                                if '[' in reference['name'] and '(' in reference['name']:
                                    url = reference['name'].split('(')[1].strip(')')
                                else:
                                    url = reference['name']
                            url = reference['source']
                        new_vulnerability.links.append(
                            {
                                'name': name,
                                'url': url
                            }
                        )

                    # Set message later in case name changed above
                    new_vulnerability.message = f'{new_vulnerability.name} in {new_dependency.file}'

                    # Append each vulnerability only once
                    new_vuln_id = f'{new_vulnerability.name}-{new_dependency.file}'
                    if new_vuln_id not in self.added_vulns:
                        self.vulnerabilities.append(new_vulnerability)
                        self.added_vulns.append(new_vuln_id)

    def output_gitlab_report(self):
        """Write the vulnerabilities to a new JSON file."""
        gitlab_report = {
            'version': '2.3',
            'vulnerabilities': [],
        }
        for vulnerability in self.vulnerabilities:
            gitlab_report['vulnerabilities'].append(vulnerability.__dict__)

        with open(f'{os.getcwd()}/gl-dependency-check-report.json', 'w') as outfile:
            json.dump(gitlab_report, outfile, indent=4)

        print('Report conversion completed successfully')


class Dependency:
    """Key information about the dependency."""
    id = None
    name = None
    file = None


class Vulnerability:
    """Each dependency may have multiple vulnerabilities."""
    category = None
    name = None
    message = None
    description = None
    severity = None
    # confidence = 'Undefined'
    scanner = dict()
    location = dict()
    identifiers = list()
    links = list()


if __name__ == '__main__':
    rc = ODCtoGitLabConverter()
    rc.processs_depcheck_report()
    rc.output_gitlab_report()
