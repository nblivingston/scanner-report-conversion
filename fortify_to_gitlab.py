#!/usr/bin/env python3

"""
Converts a Fortify FPR (FVDL) report to GitLab format. The input file must be in
the directory from which the command is run, and must be named
'fortify-scan-results.fpr'. The output file will be to the same directory, and will
be named 'gl-fortify-sast-report.json'.

Requires bs4 to be installed with pip. No longer uses the lxml parser, as the lxml parser prevented
the CDATA in snippets from being detected. Although the souce is XML, the HTML parser works.

Notes about the conversion:
  - The PDF version of the Fortify report modifies the reported severity based on 'likelihood'.
    As a result, the reported severity in the PDF and in GitLab Security Center may differ.
  - The line numbers may be wrong in the output. This is likely because Fortify removes blank lines
    when translating Python files.
  - In the GitLab SAST report, 'name' and 'message' appear to always be the same.
  - The only displayed field from 'identifiers' is 'name'.
  - Identifier 'type' must be unique for each vulnerability.

Initially, this included the option to specify input and output files. This was
flagged by Fortify as a security vulnerability, so was removed.

Nick Livingston, July 2020

TODO:
- Add to package registry after GitLab 13, so access tokens can work
- Incorporate snippet into output report
- Show only the first identifier. Identifiers are concatenated in the vulnerability display.
- Remove lxml installation from image.
"""

import json
import logging
import os
from pathlib import Path
import sys
import uuid
from zipfile import ZipFile

from bs4 import BeautifulSoup


logging.basicConfig(stream=sys.stdout, level=logging.INFO)


CONFIDENCE_MAPPING = {
    '5.0': 'Confirmed',
    '4.0': 'High',
    '3.0': 'Medium',
    '2.0': 'Low',
    '1.0': 'Unknown'
}
# Adjusted after an example produced a 5.0, no idea what that means
SEVERITY_MAPPING = {
    '5.0': 'Unknown',
    '4.0': 'Critical',
    '3.0': 'High',
    '2.0': 'Medium',
    '1.0': 'Low'
}


class FortifyToGitLabConverter:
    """Converts a Fortify Report to GitLab format."""
    descriptions = dict()
    fpr_path = 'fortify-scan-results.fpr'
    # Can't write to /opt/hp_fortify_sca/bin/
    #gitlab_report_path = f'{Path(__file__).parent.absolute()}/gl-fortify-sast-report.json'
    gitlab_report_path = os.path.join(os.getcwd(), 'gl-fortify-sast-report.json')
    snippets = dict()
    soup = None
    vulnerabilities = list()

    def __init__(self):
        """Load the FPR as a BeautifulSoup object."""
        if not os.getenv('CI_SERVER_HOST'):
            # Running locally
            self.fpr_path = f'{Path(__file__).parent.absolute()}/fortify-scan-results.fpr'
        else:
            if len(sys.argv) == 2:
                self.fpr_path = sys.argv[1]

        self.fpr_to_soup()

    def fpr_to_soup(self):
        """Extracts the FVDL from an FPR archive."""
        with ZipFile(self.fpr_path, 'r') as fpr_zip:
            with fpr_zip.open('audit.fvdl') as fvdl_file:
                self.soup = BeautifulSoup(fvdl_file, 'html.parser')

    def run(self):
        self.get_snippets()
        self.get_descriptions()
        self.get_vulnerabilities()
        self.export_json()
        logging.info('Report conversion completed successfully')

    def get_snippets(self):
        """Create a dictionary of the code snippets in the report."""
        for snippet in self.soup.find_all('snippet'):
            id = snippet['id'].split('#')[0]
            text = None
            for tag in snippet.contents:
                if tag.name == 'text':
                    text = tag.text.strip('\n')
                    break
            self.snippets.update({id: text})

    def get_descriptions(self):
        """Create a dictionary of all the class descriptions."""
        for desc in self.soup.find_all('description'):
            if '<AltParagraph>' in desc.abstract.text:
                abstract = desc.abstract.text.split('<AltParagraph>')[1]
                abstract = abstract.split('</AltParagraph>')[0]
            else:
                abstract = desc.abstract.text
            if '<Content>' in abstract:
                abstract = abstract.split('<Content>')[1]
                abstract = abstract.split('</Content>')[0]
            new_desc = {
                desc['classid']:
                    {
                        'abstract': abstract,
                        'identifiers': self.get_identifiers(desc.references)
                    }
                 }
            self.descriptions.update(new_desc)

    def get_identifiers(self, references):
        """Parse the References for a Description."""
        identifiers = list()
        identifier_count = 0
        for reference in references.contents:
            if hasattr(reference.title, 'text'):
                name = reference.title.text
                if hasattr(reference, 'author'):
                    if reference.author is not None:
                        name += f', {reference.author.text}'
                identifier = {
                    'type': str(uuid.uuid4()),
                    'name': name,
                    'value': 'security vulnerability',
                }
                if hasattr(reference, 'source'):
                    if reference.source is not None:
                        url = reference.source.text
                        identifier.update({'url': url})

                identifiers.append(identifier)
            identifier_count += 1
            if identifier_count > 2:
                break
        return identifiers

    def get_vulnerabilities(self):
        """Parse the soup into Vulnerability objects."""
        if len(self.snippets) == 0:
            self.get_snippets()
        if len(self.descriptions) == 0:
            self.get_descriptions()

        for vuln in self.soup.find_all('vulnerability'):
            classid = vuln.classinfo.classid.text
            kingdom = vuln.classinfo.kingdom.text
            new_vuln = Vulnerability()
            # Assign attributes
            new_vuln.classid = classid
            new_vuln.id = vuln.instanceinfo.instanceid.text
            new_vuln.category = 'sast'
            # Combine type and subtype to create the name
            new_vuln.name = vuln.classinfo.type.text
            if hasattr(vuln.classinfo.subtype, 'text'):
                new_vuln.name += f': {vuln.classinfo.subtype.text}'
            # Message and name appear to always be the same in GL reports
            new_vuln.message = new_vuln.name
            new_vuln.description = self.descriptions[classid]['abstract']
            new_vuln.cve = 'Deprecated'
            new_vuln.severity = SEVERITY_MAPPING[vuln.instanceinfo.instanceseverity.text]
            # Sometimes the confidence is a decimal, such as '4.8'
            confidence = vuln.instanceinfo.confidence.text
            if not confidence[-1] == '0':
                confidence = round((int(confidence.replace('.', '')))/10)
                confidence = f'{confidence}.0'
            new_vuln.confidence = CONFIDENCE_MAPPING[confidence]
            new_vuln.scanner = {
                'id': 'fortify_sca',
                'name': 'Fortify SCA'
            }

            node = None
            for entry in vuln.analysisinfo.unified.trace.primary.contents:
                if hasattr(entry, 'node'):
                    node = entry.node

            if 'label' in node.attrs:
                label = node['label']
            else:
                # Look for the snippet
                snippet_id = node.sourcelocation['snippet'].split('#')[0]
                label = self.snippets[snippet_id]

            # Sometimes "sourcelocation" is not present
            if node.sourcelocation:
                # Sometimes "lineend" is not present
                if 'lineend' in node.sourcelocation.attrs.keys():
                    lineend = int(node.sourcelocation['lineend'])
                else:
                    lineend = int(node.sourcelocation['line'])

                new_vuln.location = {
                    'file': node.sourcelocation['path'],
                    'start_line': int(node.sourcelocation['line']),
                    'end_line': lineend,
                    'class': kingdom,
                    'method': label,
                    'dependency': {
                        'package': {}
                    }
                }
                new_vuln.identifiers = self.descriptions[classid]['identifiers']
                delattr(new_vuln, 'classid')
                self.vulnerabilities.append(new_vuln)

    def export_json(self):
        """Writes the vulnerabilities dictionaries to a GL SAST-formatted file."""
        if len(self.vulnerabilities) == 0:
            self.get_vulnerabilities()

        gitlab_report = {
            'version': '2.4',
            'vulnerabilities': [],
            'remediations': []
        }
        for vulnerability in self.vulnerabilities:
            gitlab_report['vulnerabilities'].append(vulnerability.__dict__)

        with open(self.gitlab_report_path, 'w') as outfile:
            json.dump(gitlab_report, outfile, indent=2)

        print(f'{self.gitlab_report_path} saved!')


class Vulnerability:
    classid = None
    id = None
    category = None
    name = None
    message = None
    description = None
    cve = None  # Deprecated, still required
    severity = None
    confidence = None
    scanner = None
    location = None
    identifiers = dict()


if __name__ == '__main__':
    f2gl = FortifyToGitLabConverter()
    f2gl.run()
